=== WP2LEADS |  WordPress und KlickTipp einfach verbinden - WooCommerce und KlickTipp einfach verbinden ===
Contributors: Tobias_Conrad
Tags: marketing automation, woocommerce emails, lead generation, upsell, email automation
Tested up to: 6.7
Requires PHP: 7.0
Stable tag: 3.4.5
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Wie du deine Lieblings-Plugins wie WooCommerce, WebinarIgnition, Contact Form 7, Learndash usw. mit dem E-Mail-Marketing-Service KlickTipp verbindest.

== Beschreibung ==
Der Vorteil dieses Plugins: Es bietet dir Verbindungsvorlagen, damit du z.B. WooCommerce, Contact Form 7 usw. mit wenigen Klicks mit Klick-Tipp verbinden kannst.
Sollte keine Verbindungsvorlage vorhanden sein, kannst du fast alle Plugins* mit KlickTipp verbinden oder unseren "Done4u"-Service nutzen, bei dem wir es für dich die Verbindung herstellen.
[NEUE Homepage 2024 -> Hier findest du alle WordPress-Plugins, die WP2LEADS mit Klick-Tipp verbindet](https://wp2leads.com)

= Ein Comic das die WP2LEADS Vorteile für dich erklärt =

https://www.youtube.com/watch?v=IFxzAb_at3s

= Führung durchs Plugin 2024.12 = 

https://www.youtube.com/watch?v=OnOZmB4vJbU

= Da ein großer Teil des Plugins die WooCommerce-Automatisierung mit KlickTipp ist, hier ein Videos dazu 2024.12  = 

https://www.youtube.com/watch?v=3B5RpZzBKXs

= Neues Handbuch seit 2022 =

[Jetzt zugreifen und besser und schneller verbinden](https://wp2leads.tawk.help/de)

= Verfügbare Verbindungen =

* Verbinde ganz einfach das Premium-Buchungs- und Terminplanungs-Plugin Amelia mit KlickTipp.
Besonderheit dieser Verbindung: Die Daten werden in Echtzeit übertragen und können beliebig oft erneut übertragen werden. Inhalte von Zusatzfeldern können als Tags oder Feldwerte in KlickTipp übertragen werden.

* Verbinde deinen WooCommerce-Shop einfach mit Klick-Tipp.
Sende nahezu alle Daten deiner Produkte und Kunden als Tags oder Feldwerte an KlickTipp.

* Nutze diese Erweiterungen, um deinen WooCommerce-Shop mit KlickTipp zu automatisieren:
**Bewertungsautomation** (10-15% Bewertungs-Conversion und 4-5 Sterne Bewertungen filtern) inklusive KlickTipp Bewertungskampagne.
**Woocommerce Subscription** (Angebote von Abonnements z.B. für Mitgliederbereiche),
**Woocommerce to Klick-Tipp LeadValue** (summiert den gesamten Wert eines Kunden und überträgt den CustomerLeadValue an KlickTipp),
**Woocommerce Simple Coupon** to Klick-Tipp (Gutschein je E-Mail Adresse),
**Woocommerce Limit Purchase** (Kostenlose Produkte nur einmal verschenken)

* Verbinde Gravity Forms ganz einfach mit KlickTipp.
Besonderheit: Daten von beliebigen Add-ons können mit übertragen werden.

* Die beiden folgenden KlickTipp-Formular-Plugins sind vollständig angebunden, d.h.
Weiterleitung nach dem Eintragen je nach Kontaktstatus (bestätigt oder nicht bestätigt).
**NEX Forms einfach mit KlickTipp verbinden.**
Besonderheit: **PDFs können direkt aus Formulardaten generiert** werden und als Link oder HTML einfach an Klick-Tipp gesendet werden. PDFs werden nicht gespeichert, sondern beim Klick auf den Link generiert (gut für den Datenschutz).
**Feldsätze können mehrfach gefüllt werden**, wenn der vorherige Satz an Feldern bereits gefüllt ist. Löschen des Feldsatzes mit einem Link.
Ich finde, dass NEX Forms schnell schöne und funktionelle Formulare erstellt.
**Contact Form 7 CF7 einfach mit KlickTipp verbinden.**
Tags aus Drop-Downs und Multiple Choice-Fragen werden mit einem Knopfdruck generiert.
Damit sind große Umfragen möglich, bei denen jede Antwort in Klick-Tipp als Tag gespeichert wird.

* Verbinde WebinarIgnition Evergreen & Live & Instant Webinare einfach mit KlickTipp.
Einmalige Zahlung statt monatlich 69 € (auch günstiges Abo verfügbar).
**Das wohl umfangreichste Webinar Plugin**, das jetzt auch **Multiple Call-To-Actions (Verkaufs-Booster)** beinhaltet. Besonders ist, dass du die **Webinarseiten** über Shortcodes in deinem Lieblings-Page-Builder **anordnen und gestalten** kannst.
Wir haben uns nach der **Vollintegration von WebinarIgnition mit KlickTipp** in das Webinar Plugin verliebt und es Anfang 2021 gekauft.
Vollintegration: Nach Registrierung Weiterleitung je nach Kontaktstatus (bestätigt/ nicht bestätigt). Onboarding durch fertige Kampagnen und Status-Tags und -Felder je nach Status möglich. Du weißt einfach alles über deine Webinar-Besucher: Wann sind sie eingestiegen, wann sind sie ausgestiegen, wie lange waren sie im Webinar, waren sie im Webinar, als der CTA ausgespielt wurde? ...
NEU! Halte dein Live Webinar mit Jitsi Meet oder Zoom Meetings ab, ...

* Verbinde LearnDash LMS einfach mit KlickTipp.
Wo manche LMS-Systeme an Leistung verlieren, bleibt LearnDash performant sowohl für den Besucher als auch für den Admin. LearnDash LMS einfach mit KlickTipp verbinden bedeutet, dass du **in Echtzeit mitbekommst, wenn dein Lernender Fortschritte macht oder nicht**, und passendes Coaching anbieten kannst...

* Verbinde "Bessere Bewertungen für WooCommerce" einfach mit KlickTipp.
Verbinde Trusted Shops einfach mit KlickTipp.

* Verbinde die Terminvereinbarungs-Software EasyAppointments einfach mit KlickTipp.
Das Plugin ist kostenlos oder du kannst für 60 € in 2 Jahren alle Funktionen (Outlook-Synchronisierung, ...) erhalten.
Einfache Terminvereinbarung für kleine Teams, die ihre Terminvereinbarung mit KlickTipp durchführen möchten.

* Verbinde Business Directory einfach mit KlickTipp.
Alle deine Kontakte, sowohl kostenlose als auch bezahlte Einträge, in einem Verzeichnis wie z.B. hier: https://oekoblog.info/branchenverzeichnis/
**Mache aus kostenlosen Einträgen bezahlte Einträge, indem du Business Directory mit KlickTipp verbindest.**

= Story Warum WP2LEADS: Problem und Lösung =

Im Jahr 2014 habe ich mit dem ehemaligen Plugin namens WOO-EMI begonnen, WooCommerce mit Klick-Tipp zu verbinden. Es war fest codiert und konnte nur so verwendet werden, wie es entwickelt wurde. Es hat nur Produkt- und Kategorie-Tags übertragen (d.h. Post-its, die für Marketingkampagnen verwendet wurden).

Das Problem war, dass bei fast jeder Kundenanfrage neue Funktionen entwickelt und fest codiert werden mussten, wie z.B. das Senden von Daten bei einem anderen Bestellstatus als nur "wc-completed".
Oder Kunden sagten: "Wir benötigen mehr übertragene Daten wie die Zahlungsart, die Produktvariante, den Bestellbetrag, das Bestelldatum, ...

So entstand Anfang 2018 die Idee, eine flexible Software zu erstellen, die für fast alle Plugins verwendet werden kann, die klar in der WP-Datenbank schreiben, wie z.B. WooCommerce, EasyAppointments, Contact Forms Entries, Better Reviews for WooCommerce, WordPress-Benutzer, Business Directory usw.

WP2LEADS wurde geboren und ermöglicht es dir, deine wertvollen Benutzerdaten flexibel in Klick-Tipp-Felder und Tags zu übertragen.
Diese Tags und Feldwerte wie Terminzeitpunkt, Art des Dienstes/Produkts können dir helfen, deine zielgerichtete E-Mail-Marketing-Automatisierung zu starten.

= Deine Hauptvorteile =

* Übertrage aktuelle Daten, starte mit vielen Daten
* Übertrage unbegrenzt Daten
* Übertrage alle verfügbaren Daten

= Flexibel beim Senden von Daten an Klick-Tipp zu Tags und Feldern =

* Tags, die für Daten oft verwendet werden, z.B. "CF7 Form gesendet", Bestellstatus "wc-completed"
(Was sagst du hier? Auf Deutsch direkt übersetzt: Tags, die für Daten genutzt werden, werden häufig genutzt wie "CF Form Send"....)
* Felder werden für individuelle Daten des Benutzers verwendet, z.B. "persönliche Nachricht", Datum wie "Geburtstag" oder Termin oder der HTML-Inhalt, der die individuelle Bewertungsanfragen-E-Mail-Vorlage enthält.
(Was sagst du hier? Auf Deutsch direkt übersetzt: Felder sind benutzte Daten, wenn es individuell für den Nutzer ist, wie eine "persönliche Nachricht", Daten wie "Geburtstag" oder "Termin oder der HtML-Inhalt, der individuelle EMail-Bewertungsanfragen Vorlagen enthält")

= Meilensteine =

* Willkommens- und Tutorial-Seite im Plugin
    Dein Vorteil: Alle Informationen an einem Ort

* Hinzugefügter Katalog-Tab
    * Katalogelemente enthalten
        * Beispiel-Formulare
        * WOW Styler
        * Erforderliche Plugins
        * Einrichtung einer Verbindung
        * Link zum Einstellungs-Assistenten mit integrierten Klick-Tipp-Kampagnen
    Deine Vorteile: Einfache Einrichtung des Formulars, der Verbindung und der Marketingkampagne
    * Es ist auch möglich, Katalogelemente hochzuladen und zu teilen.
    Deine Vorteile: Du erhältst viele Dankesnachrichten von deinen Kunden, und sie möchten, dass du das Beispiel entsprechend ihren Bedürfnissen anpasst.
    * Integrierte Plugins:
        * Der WOW Styler https://wordpress.org/plugins/cf7-styler/ im Katalog beim Herunterladen eines Formular-Katalogelements
    Deine Vorteile: Öffne dein Formular auf deiner Website, klicke auf "Stil" und beginne sofort, dein Formular zu gestalten

        * Das WooCommerce Reviews Plugin https://wordpress.org/plugins/more-better-reviews-for-woocommerce/
        Deine Vorteile: Neben einer einfachen Einrichtung erzielst du Google-Bewertungssterne. Außerdem erhöhen Bewertungen deiner Produkte das Vertrauen deiner Kunden in deine Produkte und dein Unternehmen.

* "Wp -> KT" | "Map to API" Tab-Assistent
    * Schritt-für-Schritt-Anleitung durch die Einstellungen
    Deine Vorteile: Leichte Einrichtung deiner Verbindung

* Volle Unterstützung für Contact Form 7 hinzugefügt
    * Wenn du das Formular speicherst, fragt WP2LEADS dich sofort, ob du das Formular mit Klick-Tipp verbinden möchtest, mit einem Schritt-für-Schritt-Assistenten
    Deine Vorteile: Einfache Verbindung von CF7 mit Klick-Tipp
    * Wenn du das Formular aktualisierst, aktualisiert WP2LEADS die Verbindung mit Klick-Tipp mit einem kurzen Schritt-für-Schritt-Assistenten
    Deine Vorteile: Einfaches Update deiner Verbindung
    * Das Plugin leitet den Besucher je nach den Links des Klick-Tipp Opt-in-Prozesses um.
        * Du wählst den Opt-in-Prozess im "Wp -> KT" | "Map to API" Tab im Opt-in-Bereich aus.
        * Du überprüfst hier auch Bestätigungs- und "Danke"-Links.
        * Du kannst "wenn Wert ist xy" einrichten. Verwende die Bedingungen "Option Process"
    Deine Vorteile: Es ist sehr einfach und du kannst überprüfen und auswählen, wohin der Kunde nach dem Absenden eines CF7-Formulars umgeleitet wird.
    * Der WOW Styler https://wordpress.org/plugins/cf7-styler/ wurde entwickelt
    Deine Vorteile: Einfaches Styling all deiner Formulare

= Bitte lade WP2LEADS jetzt herunter, indem du in dein WP-Admin-Panel gehst, auf "Plugins", "Installieren" gehst und nach "WP2LERADS" suchst, klicke auf "herunterladen" und "aktivieren". =
Wir sehen uns in den Tutorial-Videos innerhalb des Plugins.

Vielleicht hast du gesucht nach:
WordPress-Automatisierung, WordPress-Automatisierungs-E-Mail-Marketing-Plugin, beste WordPress-Automatisierungs-Plugins, steigere die Produktivität mit 5 Tools für die WordPress-Automatisierung, WordPress-E-Mail-Plugins oder Marketing-Automatisierung für dein Lieblings-Plugin.

== Installation ==

Diese Sektion beschreibt, wie du das Plugin installierst und es zum Laufen bringst.

z.B.
1. Lade das Plugin über das 'Plugins'-Menü in WordPress herunter und aktiviere es.
2. Lade in der Tab "Maps" eine Karte von

 einem Server herunter
    z.B. Eine Karte, die in der kostenlosen Version verwendet werden kann.
3. Sieh dir deine Daten in der Tab "MapRunner" an.
4. "Map to API"-Tab:
    Verbinde deine Daten mit der Klick Tipp API, indem du Opt-in-Prozess, Felder und Tags auswählst.
    Öffne das Übertragungsmenü und klicke auf "Übertragen".
    Teste die Übertragung mit einer einzelnen Übertragung.
    Verwende "Alle im Hintergrund übertragen", um alle aktuellen Daten zu übertragen.
    Richte einen Cron-Job ein, um neue und aktualisierte Benutzer zu übertragen.

== FAQ ==

= Ist Version 3.4, 3.0 und 2.0 mit der letzten Version kompatibel? =

* Ja, vollständig kompatibel mit Version 1.x

= Wie erstelle ich meine eigene Verbindung? =

Suche nach eindeutigen Zeichenfolgen, die du im Formular eingegeben hast, von dem du die Daten übertragen möchtest.
 
    * Schritt für Schritt kannst du die Tabelle kombinieren und eine größere erstellen.
    * Filtere die Spalten und extrahiere die Benutzerdaten aus der Tabelle.
    * Nach einer Änderung kannst du die Ergebnisse unten auf der Seite überprüfen.
Erstelle Map-Tutorial

= Welche Daten sendest du an unseren Server? =

Der Server befindet sich in Deutschland und folgt der DSGVO und erhält folgende Daten von deiner Website, damit das Plugin reibungslos funktioniert:

* Installierte Plugin-Version, damit wir dich warnen können, wenn eine Version unsicher ist oder deiner Website schaden könnte (Klick Tipp-Regel).
* Bei der kostenlosen Version werden keine Lizenzdetails und URLs ausgetauscht.
* Wenn es bei einer aktiven Lizenz Zahlungsprobleme gibt, pingt das Plugin und reduziert die Lizenz auf die kostenlose Version. Bei Zahlung sorgen wir dafür, dass die Lizenz erneut aktiviert wird.
* Beim Export einer Map: Website-URL und Lizenzdetails.
* Keine Benutzerdaten, die du an Klick-Tipp übertragen hast, werden über unseren Server gesendet.
* Du hast eine sichere 1:1-Direktverbindung zwischen deinem Server und Klick Tipp.

= Brauchen wir einen speziellen Server? =

Wir haben auf Apache- und Nginx-Servern getestet, mit PHP 7.2, mit MariaDB und MySQL.
Wir empfehlen mindestens 128 MB RAM, um die Datenbankdaten problemlos lokal auf deinem Server zu jonglieren.
Klar läuft es mit PHP 8.1. 
Tipp eine Version x.x.20+ nutzen um nicht zu den Earlybirds zu gehören und WP und anderen Devs die Chance geben es mit der neuesten PHP version zu testen.

= How can I report security bugs? = 

You can report security bugs through the Patchstack Vulnerability Disclosure Program. The Patchstack team help validate, triage and handle any security vulnerabilities. [Report a security vulnerability.](https://patchstack.com/database/vdp/webinar-ignition)

= Wofür ist es noch zu gebrauchen? = 

WordPress automation, subscribe, wordpress email, WordPress automation email marketing plugin, best wordpress automation plugins, boost productivity with 5 tools for wordpress automation, Klick Tipp, Klick-Tipp, KlickTipp, email marketing, marketing automation, lead generation, autoresponders, autoresponder, automation, connect, emma, e-commerce tool, lead gen, lead generation wordpress, subscribers, tags, tagged, tagging, tag based, mailtag, mail tags, sendmail, e-mail, email, marketing, e-mail marketing, email marketing, newsletter, leads, subscribe, subscription, Mail List Builder, mailing list, send newsletter, sign-up, signup, revenue, advertising, notification, import, importer, export, exporter, list building, people, users, user, WooCommerce, ecommerce, e-commerce, shop, WordPress, sales, sell, bridge, wordpress lead gen, wordpress lead generation, woocommerce upsell, woocommerce cross sell, woo commerce, woo, marketing automation, KlickTipp, Klick-Tipp

== Screenshots ==

1. Das sind einige Tags die du von der Verbindung WooCommerce und KlickTipp einfach verbinden erhältst
2. Katalog(29): Wähle deine Verbindung, Anwendung, erhalte Unterstützung per Chat, sieh dir die Tutorial-Videos an und verwende die Schritt-für-Schritt-Anleitung als Start für deinen einfachen WordPress-Automatisierungsprozess.
3. MapRunner: Sieh dir die letzten 100 Einträge deiner Benutzer an, die durch die aktuelle Map/Vorlage gefunden wurden.
4. Maps: Lade neue Verbindungen herunter und erhalte auch Updates von hier aus. Exportiere deine Verbindung auch auf den Map-Server.
5. Map to API: Verbinde deine Daten mit der Klick-Tipp-API, indem du Opt-in-Prozess, Felder und Tags auswählst.
6. Statistiken: Zeige den Namen der Verbindung mit der letzten übertragenen Datenmenge durch Cron oder manuell oder sofort an.
7. Map Builder: Erstelle deine eigene Map und extrahiere alle gewünschten Benutzerdaten aus der Datenbank und verbinde diese Daten in der "Map to API" / "WP -> KT"-Tab mit Klick-Tipp.
8. Tools: Lösche Tags einfach in Massen. Normalerweise müsstest du das einzeln machen und jedes Mal bestätigen.
9. Einstellungen: Füge Klick-Tipp-Anmeldeinformationen hinzu, verwalte Lizenzdetails, bewege Lizenzen und überprüfe die Plugin-Umgebung.

== Changelog ==

= 3.4.5 - 09.02.2025 =

= Erfolgreiches 2025 und auf gute Verbindung =

* Freemius SDK update, nachdem Bestätigen geniale E-Mail-Abfolge
* Demnächst: WebinarIgnition V4 - Aktualisierung der Anbindung

= Freie Fragen und Antworten Webinare montags 12:00 = 

[Jetzt registrieren und verbinden](https://wp2leads.com/#webinare)


= 3.4.4 - 31.12.2024 =

* 2 Warnungen gelöst 

= 3.4.3 - 23.12.2024 =

* Eine weitere Sicherheitslücke wurde geschlossen und wird nachdem 15.01.2024 durch Patchstack veröffentlicht. 
Daher alle aktualisieren.
[Details auf PatchStack: ](https://patchstack.com/database/report-preview/996e01b5-6a7a-410e-a30d-23afa2fe8d61?pin=tjkLXWa829vU1u2O)

* Status: 
Patch validated. This report is in the queue for disclosure and will be published at any time.
DE: Patch bestätigt.(d.H. Lücke in Version 3.4.3 geschlossen) Dieser Bericht befindet sich in der Warteschlange zur Veröffentlichung und wird jederzeit veröffentlicht.

* [NEU! WooCommerce Verbindung die Vorteile mit Detail mit vielen Screenshots](https://wp2leads.com/woocommerce-und-klicktipp-verbinden/)


= 3.4.2 - 10.12.2024 =

* Leider wurden pro Übertragungsmodule nicht erkannt und vom 5.12. - 10.12. deaktiviert.
Bitte Lead value, Bestellanzahl, FooEvents in diesem Zeitraum erneut übertragen.

= 3.4.1 - 10.12.2024 =

* Meldung über neue Ultimate Lizenz Version als gobale Meldung

[Alle Vorteile mit Detail Screenshots](https://wp2leads.com/woocommerce-und-klicktipp-verbinden/)
[Verkaufsseite mit Upgrade/Verrechnungs-Link](https://wp2leads.com/#preise)

= 3.4.0 - 04.12.2024 =

= Alle Bitte aktualisieren = 

Achtung gefundene Lücken werden behoben und nach einiger Zeit veröffentlicht.
Was noch fehlt ist das Update auf deiner Seite oder deines Kunden.

Bitte aktualisiere 
WP2LEADS auf 3.4.0 
WooCommerce Anbindung auf 1.5.0

* Neueste Sicherheits- und Stabilitätskorrekturen.
* Anpassung der freien Version an die Regeln von WordPress.org.
* Update des Hintergrunddienstes von 2018 → 2024
Dieser ist für Massenübertragungen und bald für fast alle Übertragungen zuständig.
* Die Sicherheit/Stabilität wurde weiter verbessert: Unglaublich, was zwei Zeilen Code bewirken können. Danke an den Entdecker der Sicherheitslücke.
Hintergrund Wir haben uns vor einiger Zeit mit WP2LEADS bei Patchstack angemeldet und nun gibt es fleißige Bug Hunter, die Sicherheitslücken finden.
Nun werden immer mal wieder Lücken gefunden. Das ist normal siehe Microsoft, WordPress, iOS, WooCommerce. andere Plugins Patches.
Generell wird mehr auf Sicherheit geachtet und geprüft vom WP.org Team, Patchstack, Wordfence, ect.

= Wir haben wie versprochen die neue Ultimate WooCommerce Anbindung implementiert =

Jeder, der ernsthaft E-Mail-Marketing betreibt, muss auf die neue Anbindung umstellen.
Tippe die alte Anbindung in den linken Teil des Bildschirms und die neue Anbindung in den rechten Teil.
In der Regel muss nur das Tag-Präfix und die Anpassungen kopiert werden.

* Schneller
* Weniger Server-Ressourcen, wenn du viele Mails versendest, bist du auf der sicheren Seite.
* Schnellere Verbindung
* Saubere Tags (keine Filter notwendig)
* Bessere Variationen mit Variations-Präfix
* Mehr Integrationen 
WPML (Sprachunterstützung)
kostenloses Checkbox-Plugin (Checkboxen einbauen und Tag an KlickTipp senden)
* Mehr Informationen auf der Übersichtsseite:

[Alle Vorteile mit Detail Screenshots](https://wp2leads.com/woocommerce-und-klicktipp-verbinden/)
[Verkaufsseite mit Upgrade/Verrechnungs-Link](https://wp2leads.com/#preise)

= Anleitung zum Plugin 2024.12 =

https://www.youtube.com/watch?v=OnOZmB4vJbU

= Die neue WooCommerce Anbindung 2024.12 =

https://www.youtube.com/watch?v=3B5RpZzBKXs

= Ausblick 2025: =

* Komplettes Billing langfristig über Freemius Inc.
Dort kommt alles aus einer Hand und wir kümmern uns "nur" um die Funktionen.
Upgrade Coupons/Gutscheine auf Anfrage Dann kann die bezahlte Zeit mit einem großzügigen Gutschein verrechnet werden.
Damit ist ein sofortiger Wechsel möglich.
* Massentransfer bald auch für fast jeden Transfer.
Das beschleunigt dann die Bearbeitung in WooCommerce pro Bestellung von 11 Sekunden auf 4 Sekunden beim Speichern einer Bestellung.
Und pro Bestellung bei Massenänderung: Bei 30 Bestellungen statt 330 Sekunden nur 120 Sekunden bis man weiterarbeiten kann.
Auch der Wechsel von der Kasse zur Dankeseite wird schneller.

= 3.3.4 - 18.11.2024 =

* Security fix: es ist unglaublich was eine Zeile code bewirken kann. Danke dem Finder des Sicherheitslochs.
* Update Freemius das bald unser neues Lizenzsystem für die neue WooCommerce Anbindung sein wird.
Freemius is bei den anderen Plugins schon seit 2021 im Einsatz.

= Wir gehen jetzt wie versprochen die neue WooCommerce Anbindung an =

= 3.3.3 - 30.08.2024 =

= Ausblick: komplett neue WooCommerce Anbindung = 

* HPOS und bisherige Datenbanktabellen können
* ist schneller
* liefert sauberere Tags
* Variation Tags können vorgeneriert werden

* Voraussetzungen im Haupt-Plugin sind mit diesem Release geschaffen
* Bitte aktualisieren Sie auch das "Wp2Leads Instant transfer module for WooCommerce Orders".
Dies behebt auch einen kleinen Fehler mit zusammengezogenen Tags bei Bestellungen mit mehr als einem Produkt.
Danke an Holger (3) für den Hinweis. 
* Neue Webseite von Holger (3er) zur WOO-Anbindung https://www.shop-welten.de/

= Wann wird geliefert? = 

Sobald die neue Verbindung frei von Altlasten ist, 
vollständig getestet und noch einfacher zu bedienen ist.

= Es fehlen noch kleine Verbesserungen an der jetzigen HPOS Anbindung: =.

Eine Hilfe, wenn die WooCommerce HPOS Verbindung keine Daten anzeigt, obwohl HPOS Tabellen gefüllt werden:
Im Plugin die Verbindung öffnen, in den Templatebereich gehen und dort die Verbindung einmal speichern.
Dann sind die Daten dauerhaft verfügbar.
Wir arbeiten daran, dass die Verbindung sofort funktioniert.

= 3.3.2 - 08.08.2024 =

* WordPress das in Unterordnern installiert ist, wie domain.de/wp/ , wird jetzt unterstützt.
* Server die die PHP Funktion putenv ausgeschaltet haben werden jetzt unterstützt.
* Freemius SDK update

Es fehlen noch kleine Verbesserungen.

Eine Hilfe falls die WooCommerce HPOS Verbindung keine Daten anzeigt, obwohl HPOS Tabellen befüllt werden:
Im Plugin, die Verbindung öffnen und in den Vorlagenbauplatz gehen und dort die Verbindung einmal Abspeichern.
Dann sind die Daten für immer verfügbar.
Wir arbeiten an einer sofortigen Funktion der Verbindung.

= 3.3.0 - 09.06.2024 =

= Generalüberholung =

= Geschwindigkeit erhöht, Wartezeiten reduziert, einfacher, dank Vlad =

* Die Zeit zum Öffnen der Verbindung ist extrem verkürzt.
* Editieren der Verbindung ist super schnell
* 3+ neue Buttons rechts in der Mitte zeigen Tags, Speichern, Speichern und Beenden.
* Wenn man nach unten scrollt, erscheint der Tags Vorschau Button.
Dieser zeigt die zu übertragenden Tags mit den aktuellen Einstellungen.
* Bei der ersten Einrichtung konnten im PopUp kein Kontakt übertragen werden, dies ist nun möglich.
* Felder können schnell über das neue Dropdown-Menü ausgefüllt werden
* Freemius für das Opt-in in die Onboarding-Mailingliste wurde aktualisiert.

Überprüfe bitte, ob die Übertragung automatisch funktioniert.
Bei Vlad local ging es immer, bei mir erst nach diesem Update.
Ggf. fehlende Daten bitte nachladen.
Die Überprüfung geht so:
Im WP2LEADS Reiter "Statistik" findet ihr die letzten Instant-Übertragungen.
Wann war die letzte instant Übertragung? Aktuell? Dann ist nichts weiter zu tun.

Nachladen geht so:
Öffne die WOO Verbindung und dann "Übersicht & Übertragung".
dann "Alle Übertragen" un im linken Feld Datum auswählen, 
einen Tag vor der letzten Übertragung auswählen und starten. 
Dann wird alles im Hintergrund übertragen und neue werden sofort übertragen.

Danke für deine Mühe Tobias.

Gerne könnt ihr im Chat im Plugin Feedback geben.

Danke Vlad für die gute Zusammenarbeit seit 2018.

== Upgrade Notice ==

Will work smooth when update from 1.1.11 to 2.x
